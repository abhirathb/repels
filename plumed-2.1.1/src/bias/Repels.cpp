/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   Copyright (c) 2011-2014 The plumed team
   (see the PEOPLE file at the root of the distribution for a list of names)

   See http://www.plumed-code.org for more information.

   This file is part of plumed, version 2.

   plumed is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   plumed is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with plumed.  If not, see <http://www.gnu.org/licenses/>.
   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
#include "Bias.h"
#include "ActionRegister.h"
#include "tools/Communicator.h"
#include <stdio.h>
#include <vector>
#include <math.h>
#include <cmath>
#include <string.h>
#include <cstring>
//#include <mpi.h>
#include <time.h>
#include <assert.h>
/* #define PI 3.1415926535897932*/
#define MIN(x,y) ((x)<(y)?(x):(y))
#define MAX(x,y) ((x)>(y)?(x):(y))
#define SIGN(a, b) ((b) >= 0.0 ? fabs(a) : -fabs(a))
#ifdef __PLUMED_HAS_MATHEVAL
#include <matheval.h>
#endif
/*
Module to help run the multiple-repeling-replica scheme
All parameters explained alongside their declaration
*/

using namespace std;

namespace PLMD{
    namespace bias{

        class Repels : public Bias{
            //input parameters
            int id;
            int stage_length;
            float num_lambda_changes;
            float delta_lambda;
            int lambda_step_length;
            int num_args,num_sim;
            float lambda;
            string period_str;
            int plumed_pos;
            std::vector<double> forces;

            double bias,work;

            //output variables
            Value* valueBias;
            Value* valueWork;

            void* evaluator;
            void* evaluator_deriv;
            string func;
            string var;
            char* names;

            int rank, size;
            std::vector<double> positions;
            std::vector<vector <double> > positionsAll;
            std::vector<double> period;

            void getForce();
            void getData();

            int stage;

            public:
            Repels(const ActionOptions&);
            void calculate();
            static void registerKeywords(Keywords& keys);
        };

        PLUMED_REGISTER_ACTION(Repels,"REPELS")

            void Repels::registerKeywords(Keywords& keys){
                Bias::registerKeywords(keys);
                keys.use("ARG");
                keys.add("compulsory","ID","0","simulation ID");  //each config file has to have a different ID for a system
                keys.add("compulsory","FUNC","the function you wish to evaluate"); // The function that will drive the repulsion. 
                keys.add("compulsory","STAGE_LENGTH","10","number of steps in a stage"); //Stage is the length for which each replica sees other replicas as stationary. Position updates happen at the end of the stage
                keys.add("compulsory","NUM_LAMBDA_CHANGES","50","number of times lambda is to be changed"); // Lambda is changed this many times by the amount defined in DELTA_LAMBDA
                keys.add("compulsory","DELTA_LAMBDA","1.0","value by which to change lambda"); // The amount by which lambda is to be changed when it is changed
                keys.add("compulsory","LAMBDA_STAGE_LENGTH","1","number of stages after which lambda to be changed"); // The number of stages after Lambda is to be changed
                keys.add("compulsory","PERIOD","The periodicity of each argument seperated by commas"); //The periodicity of each argument seperated by commas (Nothing between commas if not period)
                componentsAreNotOptional(keys);
                keys.addOutputComponent("bias","default","the instantaneous value of the bias potential");
                keys.addOutputComponent("work","default","the instantaneous value of the work done on the system so far");
                keys.addOutputComponent("lambda","default","the instantaneous value of the LAMBDA factor");
            }

        Repels::Repels(const ActionOptions&ao):
            PLUMED_BIAS_INIT(ao),
            id(0),
            stage_length(10),
            num_lambda_changes(50), 
            delta_lambda(1.0),
            lambda_step_length(1)  // These are initialisations of the input parameters
        {
        //parsing inputs from config files. 
            parse("ID",id);
            parse("STAGE_LENGTH",stage_length);
            parse("NUM_LAMBDA_CHANGES", num_lambda_changes);
            parse("DELTA_LAMBDA",delta_lambda);
            parse("LAMBDA_STAGE_LENGTH", lambda_step_length );
            parse("FUNC",func);
            parse("PERIOD",period_str);
            int pos=0;
            int len=0;
            for(int i=0;i<period_str.length();i++)
            {
                if(period_str[i]==',')
                {
                   if(strcmp(period_str.substr(pos,len).c_str(),"NO")==0)
                    {
                        period.push_back(-1);
                    }
                    else
                    {
                        period.push_back(std::atof(period_str.substr(pos,len).c_str())); //Period is 0.0 when not specified
                    }
                    len=0;
                    pos=i+1;
                }
                else
                    len++;
            }
            if(strcmp(period_str.substr(pos,len).c_str(),"NO")==0)
            {
                period.push_back(-1);
            }
            else
            {
                period.push_back(std::atof(period_str.substr(pos,len).c_str())); //Period is 0.0 when not specified
            }
            log.printf("Period");
            for(int i=0;i<period.size();++i)
            {
                log.printf(" %f", period[i]);
            }
            log.printf("\n");

            checkRead();

            addComponent("bias"); componentIsNotPeriodic("bias"); //bias on the total composite system
            addComponent("work"); componentIsNotPeriodic("work"); //work done on the total composite system
            addComponent("lambda"); componentIsNotPeriodic("lambda"); //work done on the total composite system
            valueBias=getPntrToComponent("bias");
            valueWork=getPntrToComponent("work");

            num_args = getNumberOfArguments();
            if(num_args!=period.size()) error("The number of periods specified is not equal to the number of arguments.");
            forces.resize(num_args); // Forces on each of the CVs

            var="d";  // The repulsion function is defined in terms of the variable d that represents pairwise distance. The function is evaluated on each pair of replicas 
            names=const_cast<char*>(var.c_str());    
            evaluator=evaluator_create(const_cast<char*>(func.c_str())); // creation of the evaluator object of the MathEval Library
            if(!evaluator) error("There was some problem in parsing matheval formula "+func);
            evaluator_deriv=evaluator_derivative(evaluator,const_cast<char*>(var.c_str())); // creation of the derivative object

            //            std::fprintf(stderr, "EXPRESSION:%s\n",evaluator_get_string(evaluator) );  For logs. 

            // MPI variables
            rank = multi_sim_comm.Get_rank();
            size = multi_sim_comm.Get_size();


            num_sim = size;
            plumed_pos = 0;  // step number
            work = 0; 
            bias=0; 

            positions.resize(num_args);
            positionsAll.resize(num_sim);
            for(int i=0; i<num_sim;i++)
            {
                positionsAll[i].resize(num_args);
            }
            forces.resize(num_args);

            log.printf("A REPELS run has been initiated with the following parameters: \\
                    \n The run will be divided into stages where each stage will be %d steps long\\
                    \n Each replica will experience a pairwise repuslive force equal to lambda times %s \\
                    \n The value of lambda will increase from 0, after every %d stages for %f times by %f units. After that, the run will continue with the same value of lambda\n",stage_length, evaluator_get_string(evaluator), lambda_step_length,num_lambda_changes,delta_lambda);
        }

        void Repels::getForce() 
        {
            double cv, cv_j, diff; 
            bias=0;
            std::vector<double> d_ij;
            d_ij.resize(num_sim);
            for(int j=0;j<num_sim;j++)   //for each replica
            {
                d_ij[j]=0;
                if(id==j)  //don't  compute if they're the same
                    continue;
                for(int i=0;i<num_args;i++) //for each CV
                {
                    cv = positions[i];
                    cv_j=positionsAll[j][i];
                    diff =  cv - cv_j;

                    if(period[i]!=-1)
                    {   
                        diff -= period[i]*round(diff/period[i]);
                    }
                    d_ij[j] += pow(diff,2);
                } // compute the sum of the square of distances
                d_ij[j] = pow(d_ij[j],0.5); //take the root of that quantity to be the distance between those replicas
                bias += lambda * evaluator_evaluate(evaluator,1,&names,&d_ij[j]); //the bias has a term added with the pairwise repulsion added. evaluated in terms of the distance just computed

            }
            valueBias->set(bias);
            for(int i=0;i<num_args;i++) // for each CV
            {
                cv = positions[i];
                forces[i]=0;
                for(int j=0;j<num_sim;j++) // for each Replica
                {
                    //don't need to do the check here bcuz force will be 0 for a constant bias term anyway
                    cv_j = positionsAll[j][i];
                    diff = cv-cv_j;
                    if(period[i]!=-1)
                        diff -= period[i]*round(diff/period[i]);
                    if(d_ij[j]!=0) // so that denominator isn't 0. If you exit this loop the correct value(0) is anyway added to force. 
                    {
                        forces[i]+= ((-1*lambda*diff)/d_ij[j])*evaluator_evaluate(evaluator_deriv,1,&names,&d_ij[j]); // Force is negative of derivative of replusion function wrt distance d times the derivative of distance d wrt CV
                    }




                }
            }

        }

        void Repels::getData() //MPI function to collect all the current position data from all replicas
        {
            std::vector<double> allpos(num_sim*num_args,0.0);
      	    
            multi_sim_comm.Allgather(positions,allpos);
            for(int i=0; i < num_sim; i++)
            {
                for(int j=0; j < num_args; j++)
                {
                    positionsAll[i][j]=allpos[i*num_args+j];
                }
            }
        }

        void Repels::calculate() 
        {
            float prev_lambda;
            //get its own positions
            for(int i=0; i<num_args; ++i)
            {
                positions[i]=getArgument(i);
            }

            if(plumed_pos%stage_length==0) //if stage change is to happen
            {
                getData(); //update positions from all other replicas
                stage = (int)plumed_pos/stage_length; //compute stage number

                std::fprintf(stderr, "STAGE:%d\n",stage); //print for logs and gags
                log.printf("STAGE changed to:%d\n",stage); //print for logs and gags
                if ((num_lambda_changes)>0 && stage%lambda_step_length==0 && stage!=0) // if lambda must be changed (checking the three conditions based on input parameters )
                {
                    num_lambda_changes--;
                    prev_lambda = lambda; 
                    lambda+=delta_lambda;
                    log.printf("LAMBDA changed to:%f\n",lambda); //print for logs and gags
                    getForce(); //getForce here so that work values are only updated here
                    work += (delta_lambda*bias)/lambda;

                }
                else
                {
                    getForce(); // even if lambda isn't changed, stage change should mean getForce
                }
            }
            else
            {
                getForce(); // Even if stage doesn't change, getForce
            }
            getPntrToComponent("lambda")->set(lambda);
            if (lambda != 0) {
                for(int i = 0; i < num_args; ++i)
                {
                    const double f_i = forces[i];
                    setOutputForce(i,f_i); // This is to set the output forces as computed by getForce to the respective CVs
                    //    setOutputForce(i,-1.);
                }
            }

            valueWork->set(work);

            plumed_pos++;
        }
    }
}
