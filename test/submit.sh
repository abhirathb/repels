#!/bin/bash
#$ -S /bin/bash
#$ -cwd
#$ -pe ompi 20
#$ -q comp.q
#$ -P prabha_group
#$ -V
#rm -rf /scratch/abhirath/
rm -rf /scratch/abhirath/test
mkdir -p /scratch/abhirath/test
cp * /scratch/abhirath/test/
cd /scratch/abhirath/test
#e1xport GMX_MAXBACKUP=2000
sleep 5
export LD_LIBRARY_PATH=/home3/abhirath/local/libmatheval/lib
date
mpirun -np 5 mdrun_mpi -s prod -o traj -plumed -multi 5 -nsteps 100000
date
