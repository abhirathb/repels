# README #

This repository "repels" is for a module of plumed-2.1 to implement "Multiple Replusive Replica" method in a non-equilibrium setting. 

Only one file is added to the original Plumed-2.1,  "plumed-2.1.1/src/bias/Repels.cpp" i.e.  [https://bitbucket.org/abhirathb/repels/src/77ebc138b157c5a391c9d8270b15216650199e87/plumed-2.1.1/src/bias/Repels.cpp?at=master](https://bitbucket.org/abhirathb/repels/src/77ebc138b157c5a391c9d8270b15216650199e87/plumed-2.1.1/src/bias/Repels.cpp?at=master). So if you already have a Plumed working with your MD engine, all you have to do is to copy this file to the this location (plumed-2.1.1/src/bias/Repels.cpp in your installation) and recompile while enabling "matheval" and "mpi" of Plumed-2.1.

### What is this repository for? ###

* Quick summary
* Version: 
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact